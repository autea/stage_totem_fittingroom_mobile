'use strict';
var socket = null;
var connect_button = document.getElementById('connect-button');
var hashSpan = document.getElementById('hash');
var signup = document.getElementById('signup');
var xhttp = new XMLHttpRequest();
var reconnectHash = "";

var navigationLeft = document.getElementById('navigation-left');
var navigationRight = document.getElementById('navigation-right');
var getPhotoButton = document.getElementById('photo-button');
var getCouponButton = document.getElementById('coupon-button');
var submitPhoneNbButton = document.getElementById('submit-coupon-button');
var phoneNumber = document.getElementById('phone-number');
var photo = document.getElementById('photo');
var photoDownloadButton = document.getElementById('photo-download-button');
var backFromPhoto = document.getElementById('back-from-photo');
var backFromCoupon = document.getElementById('back-from-coupon');
var backFromErrorButton = document.getElementById('back-from-error-button');
var shareButton = document.getElementById('share-button');
var photoImage = document.getElementById('photo-image');


var getQueryString = function ( field, url ) {
    var href = url ? url : window.location.href;
    var reg = new RegExp( '[?&]' + field + '=([^&#]*)', 'i' );
    var string = reg.exec(href);
    return string ? string[1] : null;
};

var getHashFromDomain = function() {
    var domain = window.location.host.split('.').reverse();

    return domain.pop();
};

var getDomainWithoutHash = function() {
    var domain = window.location.host.split('.').reverse();

    domain.pop();

    return domain.reverse().join('.');
};

var connect = function(hash) {
    goToLoaderView();

    socket = io.connect('http://core.stage.kb.toteminteractive.io');
    if(hash){
        init(socket, hash);
    } else {
        //error.innerText = 'No hash provided. You should specify url param: "www.url.com?hash=12345"';
        console.log("onconnect");
    }
};

var emit = function(event, data, cb){
        if(socket){
           if(event === 'message'){                    
               if(data.data){
                   socket.emit(event, data);
               } else {
                   console.log('Incorrect message. It has to contain "data" property with object as a value, like {data : {}}', {event:event, data:data});
               }
           } else {
               socket.emit(event, data);
           }
            console.log('emitted: ' + event, data);
            if (cb) cb();
        } else {
            console.log('no socket available');
        }
    };

var init = function(socket, hash){
    

    var campaignId;

    var sendSignupEvent = function(nick) {
        if (signedUp) {
            emit('message', {event: 'signup', data: {username: nick}});
        }
        else {
            emit('message', {event: 'signup', data: {username: nick}},
                 function() {
                     showLeaderBoard(campaignId);
                 });
        }
        
    };

    var restartToHash = function(hash) {
        if (hash != "") 
            document.location.href = document.location.origin+document.location.pathname+'?hash='+hash;
    };

    
    var deviceConnected = Rx.Observable.fromEvent(socket, 'device-connected'); 
    var connectionFailed = Rx.Observable.fromEvent(socket, 'device-connect-error');
    var connectionRejected = Rx.Observable.fromEvent(socket, 'session-rejected');
    var reconnectHashEvent = Rx.Observable.fromEvent(socket, 'reconnect-hash');
    var messages = Rx.Observable.fromEvent(socket, 'message');
    var authenticated = Rx.Observable.fromEvent(socket, 'authenticated');
    var latencyTest = Rx.Observable.fromEvent(socket, 'latency-test');
    var receivePhoto = Rx.Observable.fromEvent(socket, 'score');
    var voucherAvailable = Rx.Observable.fromEvent(socket, 'voucherAvailable');

    latencyTest
        .subscribe(
            function(data) {
                emit('latency-ack', data);
            });
    
    connectionFailed
        .take(1)
        .subscribe(
            function(x)  {
                // error.innerText = x.err;
                console.log("onconnectionfailed");
                console.log(x.status);
                // connect_popup_error.style.display = 'block';
                socket.disconnect();

                goToConnectionErrorView();
            });
            
    connectionRejected
        .take(1)
        .subscribe(
            function()  {
                console.log("onconnectionrejecter");
                // error.innerText = 'Connections limit error'; 
                socket.disconnect(); 
                goToConnectionErrorView();
            });

    reconnectHashEvent
        .take(1)
        .subscribe(
            function(data) {
                var dataJSON = data;
                reconnectHash = dataJSON.data.hash;
            });
            
    var movement = new Rx.Subject();

    messages
        .subscribe(
            function(data) {
                try {
                    var obj = data;

                    console.log(obj, 'received message');
                    switch (data.event) {
                        case 'score': 
                                console.log("photo received");
                                var base64Header = "data:application/octet-stream;charset=utf-16le;base64,";
                                // photo.style.backgroundImage = "url('"+base64Header+data.data.photo+"')";
                                photoBackground = "url('"+base64Header+data.data.photo+"')";
                                // photoDownloadButton.setAttribute("data", base64Header+data.data.photo);                               
                                goToPhotoView();
                                break;
                        case 'voucherAvailable':
                                // getCouponButton.style.display='block';
                                // isVoucherAvailable = true;
                                // console.log('voucher available');
                                break;
                        case 'photoUrl':

                                if (/iPad|iPhone|iPod/.test(window.navigator.userAgent) && !window.MSStream) {
                                    photoDownloadButton.style.display = "none";
                                }
                                var shareUrl = "https://www.facebook.com/sharer/sharer.php?u="+data.data.url;
                                shareButton.setAttribute('data-href', shareUrl); 
                                photoDownloadButton.href = data.data.url;
                                shareButton.style.opacity = 1;
                                photoImage.src = data.data.url;
                                shareButton.onclick = function() {
                                  FB.ui({
                                    method: 'share',
                                    display: 'popup',
                                    href: data.data.url,
                                  }, function(response){});
                                }

                    }
                    campaignId = obj.data.campaignId;

                } catch(e) {
                    console.error(e, "Parsing message");
                }
            });
        
    var moves = movement
        .sample(100)
        .filter(function(x) {
            return x.rotationRate.alpha > 1; 
        });
    
    deviceConnected
        .subscribe(
            function()  {
                console.log("connected");
                goToNavigationView();
            });
    
    // receivePhoto
    //     .subscribe(
    //         function(data) {
                
    //         });

    // voucherAvailable
    //     .subscribe(
    //         function(){
                
    //         }
    //         )
    authenticated
        .subscribe(
            function() {
                emit('device-connect', {deviceId : getId(),
                                        hash : hash,
                                        domain: "hrat.co"});                
            },
            function(e) {
                console.error(e, "authentication error");
                // error.innerText = "cannot authenticate";
                console.log("onauthenticated");
            });

    emit('authentication', {username: 'device', password: 'device'});
};
 
var connections = Rx.Observable
    .fromEvent(connect_button, 'touchstart')
        .map(function(x) {return document.getElementById("hash").value;})
        .singleInstance();

var movingLeft = Rx.Observable
    .fromEvent(navigationLeft, 'touchstart')
    .subscribe(function(){
        emit('message', {event: 'leftClick', data: {}});
    });
var movingRight = Rx.Observable
    .fromEvent(navigationRight, 'touchstart')
    .subscribe(function(){
        emit('message', {event: 'rightClick', data: {}});
    });

var gettingPhoto = Rx.Observable
    .fromEvent(getPhotoButton, 'touchstart')
    .subscribe(function(){
        emit('message', {event: 'takePhoto', data: {}});
        goToLoaderView();
    });

var gettingCoupon = Rx.Observable
    .fromEvent(getCouponButton, 'touchstart')
    .subscribe(function(){
        phoneNumber.value = "+420";
        goToCouponView();
    });

// var downloadingPhoto = Rx.Observable
//     .fromEvent(photoDownloadButton, 'touchstart')
//     .subscribe(function() {
//         downloadPhoto()
//     });

var subbmitingPhoneNumber = Rx.Observable
    .fromEvent(submitPhoneNbButton, 'touchstart')
    .map(function(x) {return document.getElementById("phone-number").value;})
    .subscribe(function(number) {
        console.log("Submited number: "+number);
        var number = new PhoneNumber(number);
        var answer = document.getElementById("marianne_days_amount").value;
        if (answer == '') {
            $('#marianne_days_amount, .marianne_days_amount_label').addClass('warning');
            return;
        } else {
            $('#marianne_days_amount, .marianne_days_amount_label').removeClass('warning');
        }
        if (!number.isValid()) {
            $('#phone-number, .phone-number-label').addClass('warning');
            return;
        } else {
            $('#phone-number, .phone-number-label').removeClass('warning');
        }
        $('#phone-number').removeClass('warning');
        if (!document.getElementById('coupon-confirmation').checked) {
            $('.coupon-confirmation-text').addClass('warning');
            return;
        } else {
            $('.coupon-confirmation-text').removeClass('warning');
        }
        if (!document.getElementById('age-confirmation').checked) {
            $('.age-confirmation-text').addClass('warning');
            return;
        } else {
            $('.age-confirmation-text').removeClass('warning');
        }
        emit('message', {
            event: 'voucher', 
            data: {
                'phone':number.getNumber(), 
                'regionCode':getRegionCodeFromJSON(number.toJSON()), 
                'answer': answer
            }
        });
        goToCouponSubmitedView();
    });

backFromPhoto.addEventListener('touchstart', function(){
    goToNavigationView();
})
backFromCoupon.addEventListener('touchstart', function(){
    goToNavigationView();
})
// backFromErrorButton.addEventListener('touchstart', function() {
//     goToConnectView();
// })
// var restarts = Rx.Observable
//     .fromEvent(restart_button, 'touchstart')
//         .singleInstance();


var connHash = getQueryString('hash');
window.addEventListener('load', function(){
    if (!connHash) {
        connHash = getHashFromDomain();
    }

    if (connHash) {
        connect(connHash);
        // connect_button.style.display = 'none';
        // hashSpan.style.display = 'none';
        // connect_popup.style.display='none';
    }
    else {
        goToConnectView();
        connections
            .subscribe(
                function(hash)  {
                    console.log('connect', hash);
                    connect(hash);
                },
                function(e) {
                    console.log("Connection error");
                    });   
    }
});

    

    
function getId() {
    var id = localStorage.getItem('deviceId');
    if(!id || id.length !== 24){
        id = makeid();
        localStorage.setItem('deviceId', id);    
    }
    
    return id;
}

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 24; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// function fillLeaderBoard(leaders) {
//     var len = leaders.length;

//     if (len > 7) {
//         len = 7;
//     }

//     for (var i = 0; i < len; i++) {
//         var name = document.getElementById('leader-' + i);
//         var score = document.getElementById('leader-score-' + i);

//         name.innerText = leaders[i].username;
//         score.innerText = leaders[i].score;
//     }

//     leaderBoard.style.display = 'block';
// }

xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
        try {
            var leaders = JSON.parse(xhttp.responseText);

            fillLeaderBoard(leaders);
            
        } catch(e) {
            console.error(e, "parsing http response");
        }
    }
};

function showLeaderBoard(campaignId) {
    xhttp.open("GET", "http://reports.stage.kb.toteminteractive.io/v1/events/leaderboards/campaigns/"+campaignId, true);
    
    xhttp.send();
}

function getCountryCode() {
    for (var i=0; i<window.navigator.languages.length; i++) {
        if (PhoneNumber.getCountryCodeForRegionCode(window.navigator.languages[i])!=0){
            return window.navigator.languages[i];
        }
    }
    return null;
}
function getCountryPrefix(countryCode) {
    if (countryCode != null) {
        return "+"+PhoneNumber.getCountryCodeForRegionCode(countryCode);
    }
}
function showTmpPhoto() {
    if (/iPad|iPhone|iPod/.test(window.navigator.userAgent) && !window.MSStream) {
        photoDownloadButton.style.display = "none";
    }
    var base64Header = "data:application/octet-stream;charset=utf-16le;base64,";
    //photo.style.backgroundImage = "url('"+base64Header+tmpImage+"')";
    photoImage.src = "https://toteminteractive.blob.core.windows.net/57723253b530ee792d17b84e/57c5b556838140110022f69a";
    photoDownloadButton.href="https://toteminteractive.blob.core.windows.net/57723253b530ee792d17b84e/57c5b556838140110022f69a";
    // shareButton.setAttribute("data-href","https://toteminteractive.blob.core.windows.net/57723253b530ee792d17b84e/57c5b556838140110022f69a");
    shareButton.onclick = function() {
      FB.ui({
        method: 'share',
        href: 'https://toteminteractive.blob.core.windows.net/57723253b530ee792d17b84e/57c5b556838140110022f69a',
      }, function(response){});
    }
    shareButton.style.opacity = 1;
}

// function downloadPhoto() {
//     download(photoDownloadButton.getAttribute("data"),"photo_kb.jpg", "image/jpeg");
// }
function getRegionCodeFromJSON(numberJSONObject) {
    return numberJSONObject.regionCode;
}
